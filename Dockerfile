FROM openjdk:21

COPY . /app

ENV POSTGRES_USERNAME=postgres
ENV POSTGRES_PASSWORD=admin
ENV POSTGRES_HOST=localhost
ENV POSTGRES_DB=artichaut
ENV POSTGRES_PORT=5432

RUN cd /app && ./mvnw package -Dmaven.test.skip


EXPOSE 8080

CMD [ "java", "-jar", "/app/target/campus_hotel_artichaut_backend-0.0.1-SNAPSHOT.jar"]
